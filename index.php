<?php
session_start();
?>

<!DOCTYPE html>

<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale= 1.0">
    <title>DEV 2021</title>
    <link
        href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;700&family=Open+Sans:wght@300;400;700&display=swap"
        rel="stylesheet">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/base.css"> 
    <link rel="stylesheet" href="css/responsividade.css">
    <script src="js/validacoes.js" defer></script>
</head>

<body>
    <main class="container">
        <img src="./img/logo.png" alt="Logo da SmileShop" class="cabecalho__logo">
        <section class="chamada_container">
            <h1 class="chamada__titulo">
                <p>CRIE SEU E-COMMERCE</p>    
            </h1>
            <p class="chamada__texto_paragrafo"><b class="primeiro-paragrafo">Chegou a hora de criar seu e-comerce
                    com os especialistas da
                    SmileShop</b>
                Preencha o formulário abaixo que um consultor entrará em contato!</p>
                <div class="celular">
                    <img src="./img/celular.png" alt="Foto do Celular" class="foto_celular" >
                </div>
        </section>
    </main>
    <form action="lancamento.php" method="POST">
        <div class="formulario">
            <label for="nome">Nome</label>
            <input type="text" name="nome" id="nome" maxlength="60" minlength="5" required  />
        </div>
        <div class="formulario">
            <label for="email">E-mail</label>
            <input type="email" name="email" id="email" maxlength="100" required >
        </div>
        <div class="formulario">
            <label for="telefone">Whatsapp</label>
            <input type="text" name="celular" id="celular" data-mask="(00) 00000-0000" required>
        </div>
        <div class="formulario-radio">
            <input type="radio" class="radio" name="input" value="SIM" required> 
            <label class="radio-label" for="label">Eu autorizo um consultor da Smile entrar em contato via
                Whatsapp
            </label>  
        </div>
        <div class="formulario">
            <input type="submit" name="SendCadCont" class="botao" value="ENVIAR">
        </div>
    </form>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.0/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> 
</body>
</html>